from sqlalchemy import Integer, Float, Column, String

from domain.base import Base


class Redirect(Base):
    __tablename__ = "redirect"
    id = Column(Integer, primary_key=True)
    server_url = Column(String(), index=True, nullable=False)
    cdn_url = Column(String(), nullable=False)
    self_probability = Column(Float)
    probability = Column(Float)

    def to_dict(self):
        return {
            "id": self.id,
            "server_url": self.server_url,
            "cdn_url": self.cdn_url,
            "self_probability": self.self_probability,
            "probability": self.probability
        }
