import json
from contextvars import ContextVar
from typing import List
from random import choices
from urllib.parse import urlparse

import aioredis
from sanic import Sanic, response
from sanic_ext import validate
from sqlalchemy.ext.asyncio import create_async_engine, AsyncSession
from sqlalchemy.orm import sessionmaker
from sqlalchemy import select, delete

from models.server_info import ServerInfo
from domain.redirect import Redirect


app = Sanic("my_app")

cache = aioredis.from_url("redis://redis:6379", encoding="utf-8", decode_responses=True)

bind = create_async_engine("mysql+aiomysql://root:root@db/video_balancer")
_sessionmaker = sessionmaker(bind, AsyncSession, expire_on_commit=False)
_base_model_session_ctx = ContextVar("session")


@app.middleware("request")
async def inject_session(request):
    request.ctx.session = _sessionmaker()
    request.ctx.session_ctx_token = _base_model_session_ctx.set(request.ctx.session)


@app.middleware("response")
async def close_session(request, response):
    if hasattr(request.ctx, "session_ctx_token"):
        _base_model_session_ctx.reset(request.ctx.session_ctx_token)
        await request.ctx.session.close()


async def get_redirects_from_db(url: str, session) -> Redirect:
    stmt = select(Redirect).filter(Redirect.server_url == url)
    result = await session.execute(stmt)
    return result.scalars()


async def remove_redirects_from_db(url: str, session) -> None:
    stmt = delete(Redirect).where(Redirect.server_url == url)
    await session.execute(stmt)


async def check_probability_sum(hosts: List, self_prob: float) -> bool:
    return sum([url["probability"] for url in hosts]) + self_prob == 1


async def create_redirects(url: str, self_probability: float, hosts: List) -> List:
    redirects = list()
    for data in hosts:
        redirect = Redirect(
            server_url=url,
            cdn_url=data["cdn_url"],
            self_probability=self_probability,
            probability=data["probability"]
        )
        redirects.append(redirect)
    return redirects


@app.post("/redirects")
@validate(json=ServerInfo)
async def create_redirect(request, body: ServerInfo):
    session = request.ctx.session
    msg = body.dict()
    server_url = msg["server_url"]
    if not await check_probability_sum(
            msg.get("cdn_hosts", []), msg.get("self_probability", 0)):
        return response.json(
            {"message": "Probability sum is not equal to 1"}, status=409)
    async with session.begin():
        entity = await get_redirects_from_db(server_url, session)
        if entity.first() is not None:
            return response.json(
                {"message": "This server URI already exist"}, status=409)
        redirects = await create_redirects(
            server_url, msg.get("self_probability", 0), msg.get("cdn_hosts", []))
        session.add_all(redirects)
    return response.json([redirect.to_dict() for redirect in redirects])


@app.get("/redirects")
async def get_redirects(request):
    session = request.ctx.session
    async with session.begin():
        stmt = select(Redirect)
        result = await session.execute(stmt)
    return response.json([res.to_dict() for res in result.scalars().all()])


@app.put("/redirects/<url:str>")
@validate(json=ServerInfo)
async def update_redirect(request, url, body: ServerInfo):
    session = request.ctx.session
    msg = body.dict()
    if not await check_probability_sum(
            msg.get("cdn_hosts", []), msg.get("self_probability", 0)):
        return response.json(
            {"message": "Probability sum is not equal to 1"}, status=409)
    async with session.begin():
        entity = await get_redirects_from_db(url, session)
        entity = entity.first()
        if entity is not None:
            await remove_redirects_from_db(entity.server_url, session)
        redirects = await create_redirects(
            url, msg.get("self_probability", 0), msg.get("cdn_hosts", []))
        session.add_all(redirects)
    return response.json([redirect.to_dict() for redirect in redirects])


@app.delete('/redirects/<url:str>')
async def delete_redirect(request, url):
    session = request.ctx.session
    async with session.begin():
        await remove_redirects_from_db(url, session)
    return response.json({"status": "ok"})


@app.get("/")
async def handler_request(request):
    video = request.args.get("video")
    parsed_url = urlparse(video)
    hostname = parsed_url.hostname
    server_name = hostname.split('.')[0]
    cache_url = await cache.get(server_name)
    if cache_url is not None:
        redirects = json.loads(cache_url)
    else:
        session = request.ctx.session
        async with session.begin():
            redirects = await get_redirects_from_db(server_name, session)
        redirects = [redirect.to_dict() for redirect in redirects.all()]
        if len(redirects) == 0:
            return response.redirect(video)
        await cache.set(server_name, json.dumps(redirects))
    await cache.expire(server_name, 60)
    probs = [redirects[0]["self_probability"]] + [prob["probability"] for prob in redirects]

    redirect_id = choices(range(len(probs)), probs)[0]
    if redirect_id == 0:
        return response.redirect(video)

    return response.redirect(
        f"http://{redirects[redirect_id-1]['cdn_url']}/{server_name}{parsed_url.path}"
    )


@app.get("/ping")
async def ping(request):
    return response.text("pong")


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000)
