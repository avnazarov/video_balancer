from typing import List

from pydantic import BaseModel


class CDNInfo(BaseModel):
    cdn_url: str
    probability: float


class ServerInfo(BaseModel):
    server_url: str
    self_probability: float
    cdn_hosts: List[CDNInfo]
