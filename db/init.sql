CREATE DATABASE video_balancer;
use video_balancer;

CREATE TABLE redirect(
  id INT PRIMARY KEY AUTO_INCREMENT,
  server_url VARCHAR(100) NOT NULL,
  cdn_url VARCHAR(100) NOT NULL,
  self_probability FLOAT,
  probability FLOAT
);

ALTER TABLE `redirect` ADD INDEX (`server_url`);
